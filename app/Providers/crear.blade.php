@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">CREAR UN CLIENTE</div>
                <div class="col text-right">
                    <a href= "{{route('list.clientes') }}"class="btn btn-sm btn-seccess">cancelar</a>
                
                <div class="card-bady">

                <form role="form" method='post' action="{{url (guardar.clientes')}}">
                {{ csrf_field() }}
                {{ method_field('post') }}

                
            
            
             </div class="row">
                 <div class="col-lg-4">
                 <larabel class="from-control-larabel" for="nombre"> Nombre del cliente</larabel>
                 <input type="text" class="from-control" name="nombre">
            </div>

            <div class="col-lg-4">
                 <larabel class="from-control-larabel" for="apellidos"> Apellidos del cliente</larabel>
                 <input type="text" class="from-control" name="apellidos">
            </div>

            <div class="col-lg-4">
                 <larabel class="from-control-larabel" for="cedula"> cedula del cliente</larabel>
                 <input type="text" class="from-control" name="cedula">
            </div>

            <div class="col-lg-4">
                 <larabel class="from-control-larabel" for="direccion"> direccion del cliente</larabel>
                 <input type="text" class="from-control" name="direccion">
            </div>

            <div class="col-lg-4">
                 <larabel class="from-control-larabel" for="telefono"> telefono del cliente</larabel>
                 <input type="text" class="from-control" name="telefono">
            </div>

            <div class="col-lg-4">
                 <larabel class="from-control-larabel" for="fech nacimiento"> fecha nacimiento del cliente</larabel>
                 <input type="text" class="from-control" name="fecha nacimiento">
            </div>

            <div class="col-lg-4">
                 <larabel class="from-control-larabel" for="email"> email del cliente</larabel>
                 <input type="text" class="from-control" name="email">
                  
            </div>

            </div>

            <button type="submit" class="btn btn-success pull-right"> guardar 
    </div>
</div>
@endsection
